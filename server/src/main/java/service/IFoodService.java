package service;

import data.BrandedFood;
import data.Food;
import data.FoodReport;
import exceptions.ClientMessageException;

import java.util.List;

public interface IFoodService {

    public List<Food> getFoods(final String foodName) throws ClientMessageException;

    public FoodReport getFoodReport(final String fdcId) throws ClientMessageException;

    BrandedFood getBrandedFood(String fdcId) throws ClientMessageException;

//    public BrandedFood getBrandedFood(final String foodFdcId) throws InterruptedException, IOException, ServiceUnavailableException;

}

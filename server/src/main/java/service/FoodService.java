package service;

import converters.JsonConverter;
import data.BrandedFood;
import data.Food;
import data.FoodList;
import data.FoodReport;
import exceptions.ClientMessageException;
import exceptions.EmptyFoodNameException;
import exceptions.FoodApiServerException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static utils.ServerResponses.EMPTY_FOOD_NAME;
import static utils.ServerResponses.SERVER_RETRIEVE_FAILURE;

public class FoodService implements IFoodService {
    private static final String FOOD_DATA_API_URL = "https://api.nal.usda.gov/fdc/v1/";
    private static final int STATUS_CODE_OK = 200;
    private static final int DEFAULT_PAGE_NUMBER = 1;

    private final HttpClient client;
    private final String apiKey;

    public FoodService(HttpClient client, String apiKey) {
        this.client = client;
        this.apiKey = apiKey;
    }

    public List<Food> getFoods(final String foodName) throws ClientMessageException {
        if (foodName == null || foodName.isEmpty()) {
            throw new EmptyFoodNameException(EMPTY_FOOD_NAME);
        }

        final HttpResponse<String> foodDataResponse = getResponse(buildUriByFoodNameAndPageNumber(foodName,
                DEFAULT_PAGE_NUMBER));

        final FoodList foods = JsonConverter.getConvertedFoodList(foodDataResponse.body());

        if (foods.getTotalPages() > DEFAULT_PAGE_NUMBER) {
            return getFoodsFromAllPages(foodName, foods);
        }

        return foods.getFoods();
    }

    public FoodReport getFoodReport(String fdcId) throws ClientMessageException {
        final HttpResponse<String> foodDataResponse = getResponse(buildUriByFoodFdcId(fdcId));

        return JsonConverter.getConvertedFoodReport(foodDataResponse.body());
    }

    @Override
    public BrandedFood getBrandedFood(String fdcId) throws ClientMessageException {
        final HttpResponse<String> foodDataResponse = getResponse(buildUriByFoodFdcId(fdcId));

        return converters.JsonConverter.getConvertedBrandedFood(foodDataResponse.body());
    }

    // When there are a lot of pages the operation gets very slow
    // maybe we have to get 2-3 pages to have something to show to the user
    // and process all the other pages on background.
    // This way the user will have to look for something and not just wait
    // and if the same command is entered then we will return all that we have
    // gathered so far.
    private List<Food> getFoodsFromAllPages(String foodName, FoodList foods) {
        final List<CompletableFuture<String>> foodsResponses = new ArrayList<>();
        int currentPage = DEFAULT_PAGE_NUMBER;

        // Make api call for every page
        while (currentPage < foods.getTotalPages() && currentPage < 2) {
            final HttpRequest request = HttpRequest.newBuilder()
                    .uri(buildUriByFoodNameAndPageNumber(foodName, currentPage))
                    .build();

            final CompletableFuture<HttpResponse<String>> httpResponseCF = client.sendAsync(request,
                    HttpResponse.BodyHandlers.ofString());
            final CompletableFuture<String> result = httpResponseCF.thenApply(HttpResponse::body);
            foodsResponses.add(result);

            currentPage++;
        }

        // Combine all of the completable futures into one.
        CompletableFuture.allOf(foodsResponses.toArray(CompletableFuture[]::new))
                .join();

        // Add all lists of foods into one and return it.
        final List<Food> allFoods = new ArrayList<>();
        for (final CompletableFuture<String> response : foodsResponses) {
            FoodList currentFoods = JsonConverter.getConvertedFoodList(response.getNow(null));

            allFoods.addAll(currentFoods.getFoods());
        }

        return allFoods;
    }

    private URI buildUriByFoodNameAndPageNumber(final String foodName, final int pageNumber) {
        final StringBuilder uri = new StringBuilder();

        uri.append(FOOD_DATA_API_URL)
                .append("foods/search?api_key=")
                .append(apiKey)
                .append("&pageNumber=")
                .append(pageNumber)
                .append("&requireAllWords=true")
                .append("&query=")
                .append(foodName);

        return URI.create(uri.toString());
    }

    private URI buildUriByFoodFdcId(final String foodFdcId) {
        final StringBuilder uri = new StringBuilder();
        uri.append(FOOD_DATA_API_URL)
                .append("food/")
                .append(foodFdcId)
                .append("?api_key=")
                .append(apiKey);

        return URI.create(uri.toString());
    }

    private HttpResponse<String> getResponse(final URI url) throws ClientMessageException {
        final HttpRequest request = HttpRequest.newBuilder()
                .uri(url)
                .build();

        HttpResponse<String> foodDataResponse = null;

        try {
            foodDataResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            System.err.println(e.getMessage() + " cause is: " + e.getCause());
            throw new FoodApiServerException(SERVER_RETRIEVE_FAILURE);
        }

        handleHttpError(foodDataResponse);

        return foodDataResponse;
    }

    // Handle different status codes in the future
    // log with status code and error message
    private void handleHttpError(HttpResponse<String> response) throws ClientMessageException {
        if (response.statusCode() != STATUS_CODE_OK) {
            System.err.println("Problem with server response, status code is " + response.statusCode());
            throw new FoodApiServerException(SERVER_RETRIEVE_FAILURE);
        }
    }

}

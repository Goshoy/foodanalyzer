package commands;

import exceptions.ClientMessageException;
import exceptions.NoArgumentsException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static utils.ServerResponses.MISSING_ARGUMENTS;

public class CommandExecutor implements ICommandExecutor {
    public static final String WHITE_SPACE_SPLIT_REGEX = " ";
    public static final int COMMAND_INDEX = 0;
    private CommandFactory commandFactory;

    public CommandExecutor(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public String execute(String input) throws IOException, ClientMessageException {
        if (input == null || input.isBlank()) {
            return "No command entered";
        }

        CommandParser parsedCommand = parseInput(input);
        AbstractCommand command = commandFactory.getCommand(parsedCommand.command);

        return command.execute(parsedCommand.arguments);
    }

    private CommandParser parseInput(String input) throws ClientMessageException {
        List<String> args = Arrays.asList(input.trim().split(WHITE_SPACE_SPLIT_REGEX));
        String command = args.get(COMMAND_INDEX);
        List<String> cmdArgs = args.subList(COMMAND_INDEX + 1, args.size());

        if (cmdArgs.isEmpty()) {
            throw new NoArgumentsException(MISSING_ARGUMENTS);
        }

        return new CommandParser(command, cmdArgs);
    }

    // Think of better name
    private class CommandParser {
        private final String command;
        private final List<String> arguments;

        public CommandParser(String command, List<String> arguments) {
            this.command = command;
            this.arguments = arguments;
        }
    }
}

package exceptions;

public class FailedToExtractBarcodeFromImgException extends ClientMessageException {
    public FailedToExtractBarcodeFromImgException(String message) {
        super(message);
    }
}

package data;

import java.io.Serializable;
import java.util.Objects;

public class Nutrient implements Serializable {
    private static final long serialVersionUID = 7957237717296889570L;

    private final double value;

    public Nutrient(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Nutrient nutrient = (Nutrient) o;
        return Double.compare(nutrient.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}

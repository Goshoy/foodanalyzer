package commands;

import exceptions.ClientMessageException;

import java.io.IOException;
import java.util.List;

public interface Command {
    public String execute(List<String> args) throws IOException, ClientMessageException;
}

package data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class FoodList implements Serializable {
    private static final long serialVersionUID = 6440405062726270926L;

    private int currentPage;
    private int totalPages;
    private List<Food> foods;

    public FoodList(int currentPage, int totalPages, List<Food> foods) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.foods = foods;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<Food> getFoods() {
        return foods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodList foodList = (FoodList) o;
        return currentPage == foodList.currentPage &&
                totalPages == foodList.totalPages &&
                Objects.equals(foods, foodList.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currentPage, totalPages, foods);
    }

    @Override
    public String toString() {
        return "FoodList{" +
                "currentPage=" + currentPage +
                ", totalPages=" + totalPages +
                ", foods=" + foods +
                '}';
    }
}
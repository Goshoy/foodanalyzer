package dao;

import data.BrandedFood;
import data.Food;
import data.FoodReport;
import loader.BrandedFoodLoader;
import loader.FoodLoader;
import loader.FoodReportLoader;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class FoodDao implements IFoodDao {
    private static final String FILE_EXTENSION = ".txt";

    private final FoodLoader foodLoader;
    private final BrandedFoodLoader brandedFoodLoader;
    private final FoodReportLoader foodReportLoader;
    private Set<Food> foodsCache;
    private Map<String, FoodReport> foodReportCache;
    private Map<String, BrandedFood> brandedFoodCache;


    public FoodDao(FoodLoader foodLoader, BrandedFoodLoader brandedFoodLoader, FoodReportLoader foodReportLoader)
            throws IOException, ClassNotFoundException {
        this.foodLoader = foodLoader;
        this.brandedFoodLoader = brandedFoodLoader;
        this.foodReportLoader = foodReportLoader;

        loadFoods();
    }

    private void loadFoods() throws IOException, ClassNotFoundException {
        this.foodsCache = this.foodLoader.load();
        this.foodReportCache = this.foodReportLoader.load();
        this.brandedFoodCache = this.brandedFoodLoader.load();
    }

    public void cacheFood(final Food food) throws IOException {
        final String filePath = this.foodLoader.getFilePath() + File.separator + food.getFdcId() + FILE_EXTENSION;
        final Path path = Paths.get(filePath);
        cache(food, path);
        this.foodsCache.add(food);
    }

    public void cacheFoodReport(final FoodReport foodReport) throws IOException {
        final String filePath = this.foodReportLoader.getFilePath() + File.separator + foodReport.getFdcId() + FILE_EXTENSION;
        final Path path = Paths.get(filePath);
        cache(foodReport, path);
        this.foodReportCache.put(foodReport.getFdcId(), foodReport);
    }

    public void cacheBrandedFood(final BrandedFood brandedFood) throws IOException {
        final String filePath = this.brandedFoodLoader.getFilePath() + File.separator + brandedFood.getGtinUpc() + FILE_EXTENSION;
        final Path path = Paths.get(filePath);
        cache(brandedFood, path);
        this.brandedFoodCache.put(brandedFood.getGtinUpc(), brandedFood);
    }

    public List<Food> getFood(final List<String> foodName) {
        return this.foodsCache.stream().filter(food -> containsAllWords(food, foodName)).collect(Collectors.toList());
    }

    public FoodReport getFoodReport(final String fdcId) {
        return this.foodReportCache.get(fdcId);
    }

    public BrandedFood getBrandedFood(final String gtinUpc) {
        return this.brandedFoodCache.get(gtinUpc);
    }

    @Override
    public boolean containsFood(List<String> foodName) {
        return this.foodsCache.stream().filter(food -> containsAllWords(food, foodName)).count() > 0;
    }

    @Override
    public boolean containsBrandedFood(String gtinUpc) {
        return this.brandedFoodCache.containsKey(gtinUpc);
    }

    @Override
    public boolean containsFoodReport(String fdcId) {
        return this.foodReportCache.containsKey(fdcId);
    }

    private void cache(final Object object, final Path path) throws IOException {
        try (final var oop = new ObjectOutputStream(Files.newOutputStream(path))) {
            oop.writeObject(object);
            oop.flush();
        }
    }

    private boolean containsAllWords(final Food food, final List<String> words) {
        for (final String word : words) {
            if (!food.getDescription()
                    .toLowerCase()
                    .contains(word.toLowerCase())) {
                return false;
            }
        }

        return true;
    }
}

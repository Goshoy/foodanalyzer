package providers;

import dao.IFoodDao;
import exceptions.BrandedFoodNotFoundException;
import exceptions.ClientMessageException;

public class BrandedFoodProvider implements IProvider {
    private final IFoodDao foodDao;

    public BrandedFoodProvider(IFoodDao foodDao) {
        this.foodDao = foodDao;
    }

    @Override
    public String provide(String criterion) throws ClientMessageException {

        if (!foodDao.containsBrandedFood(criterion)) {
            throw new BrandedFoodNotFoundException("Branded food not found.");
        }

        return foodDao.getBrandedFood(criterion).toString();
    }
}

package utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BarcodeExtractor {

    public static String extractCode(String imagePath) throws IOException, NotFoundException {
        final File file = new File(imagePath);
        final BufferedImage bufferedImage = ImageIO.read(file);
        final LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        final BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        final Result result = new MultiFormatReader().decode(bitmap);

        return result.getText();
    }
}

package loader;

import data.BrandedFood;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrandedFoodLoader extends Loader {
    public BrandedFoodLoader(String filePath) {
        super(filePath);
    }

    public Map<String, BrandedFood> load() throws IOException, ClassNotFoundException {
        final List<Path> filePaths = super.getPaths(super.getFilePath());
        final Map<String, BrandedFood> foods = new HashMap<>();

        for (final Path p : filePaths) {
            try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(p))) {
                BrandedFood brandedFood = (BrandedFood) ois.readObject();
                foods.put(brandedFood.getGtinUpc(), brandedFood);
            }
        }

        return foods;
    }
}

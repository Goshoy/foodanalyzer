package exceptions;

public class BrandedFoodNotFoundException extends ClientMessageException {
    public BrandedFoodNotFoundException(String message) {
        super(message);
    }
}

package commands;

import providers.IProvider;

public abstract class AbstractCommand implements Command {
    IProvider provider;

    public AbstractCommand(IProvider provider) {
        this.provider = provider;
    }
}

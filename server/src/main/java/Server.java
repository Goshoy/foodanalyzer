import commands.ICommandExecutor;
import exceptions.ClientMessageException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class Server {
    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 8080;
    private static final int BUFFER_SIZE = 1024;
    private ServerSocketChannel listeningSocketChannel;
    private Selector selector;
    private final ByteBuffer buffer;
    private final ICommandExecutor commandExecutor;

    public Server(ICommandExecutor commandExecutor) {
        buffer = ByteBuffer.allocate(BUFFER_SIZE);
        this.commandExecutor = commandExecutor;
    }

    public void start() {
        try {
            initSelector();
            initListeningSocketChannel();

            while (true) {
                selector.select();
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if (!key.isValid()) { // if closed
                        continue;
                    }
                    if (key.isAcceptable()) {
                        acceptClient(key);
                    } else if (key.isReadable()) {
                        receiveFromClient(key);
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("Server failure");
        }
    }

    private void initSelector() throws IOException {
        selector = Selector.open();
    }

    private void initListeningSocketChannel() throws IOException {
        listeningSocketChannel = ServerSocketChannel.open();
        listeningSocketChannel.configureBlocking(false);
        listeningSocketChannel.bind(new InetSocketAddress(SERVER_HOST, SERVER_PORT));
        listeningSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    }

    private void acceptClient(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel clientChannel = serverSocketChannel.accept();
        clientChannel.configureBlocking(false);
        clientChannel.register(selector, SelectionKey.OP_READ);
    }

    private void receiveFromClient(SelectionKey key) {
        final SocketChannel client = (SocketChannel) key.channel();
        handleClient(client);
    }

    private void handleClient(SocketChannel client) {
        try {
            final String input = extractUserInputFromBuffer(client).trim();
            System.out.println("Recieved input " + input);

            String result = commandExecutor.execute(input);

            if (result == null || result.equals("[]")) {
                result = "Nothing found.";
            }

            sendResult(result, client);
        } catch (IOException | ClientMessageException e) {
            sendResult(e.getMessage(), client);
        }
    }

    private String extractUserInputFromBuffer(final SocketChannel client) throws IOException {
        buffer.clear();
        client.read(buffer);
        buffer.flip();
        byte[] bytes = new byte[buffer.limit()];
        buffer.get(bytes);

        return new String(bytes);
    }

    private void sendResult(final String result, final SocketChannel client) {
        final String newMessage = result + System.lineSeparator();
        final ByteBuffer byteBuffer = ByteBuffer.allocate(newMessage.getBytes().length);
        byteBuffer.put(newMessage.getBytes());
        byteBuffer.flip();
        try {
            client.write(byteBuffer);
        } catch (IOException e) {
            try {
                client.close();
            } catch (IOException ex) {
                System.err.println("There is a problem with sending the message, error message: " + e.getMessage());
            }
        }
    }
}

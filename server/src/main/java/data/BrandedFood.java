package data;

import java.io.Serializable;
import java.util.Objects;

public class BrandedFood extends Food implements Serializable {
    private static final long serialVersionUID = -1077211618579913161L;

    private final String gtinUpc;

    public BrandedFood(String fdcId, String description, String dataType, String gtinUpc) {
        super(fdcId, description, dataType);
        this.gtinUpc = gtinUpc;
    }

    public String getGtinUpc() {
        return gtinUpc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BrandedFood that = (BrandedFood) o;
        return Objects.equals(gtinUpc, that.gtinUpc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gtinUpc);
    }

    @Override
    public String toString() {
        StringBuilder brandedFood = new StringBuilder();

        brandedFood.append(super.toString())
                .append("gtinUpc: ")
                .append(gtinUpc);

        return brandedFood.toString();
    }
}

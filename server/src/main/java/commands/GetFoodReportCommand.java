package commands;

import exceptions.ClientMessageException;
import providers.IProvider;

import java.io.IOException;
import java.util.List;

public class GetFoodReportCommand extends AbstractCommand {
    public GetFoodReportCommand(IProvider provider) {
        super(provider);
    }

    @Override
    public String execute(List<String> args) throws IOException, ClientMessageException {
        return provider.provide(args.get(0));
    }
}

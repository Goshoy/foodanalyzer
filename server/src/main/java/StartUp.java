import commands.CommandExecutor;
import commands.CommandFactory;
import commands.Commands;
import commands.ICommandExecutor;
import dao.FoodDao;
import dao.IFoodDao;
import loader.BrandedFoodLoader;
import loader.FoodLoader;
import loader.FoodReportLoader;
import providers.BrandedFoodProvider;
import providers.FoodProvider;
import providers.FoodReportProvider;
import providers.IProvider;
import service.FoodService;
import service.IFoodService;
import utils.Constants;

import java.io.IOException;
import java.net.http.HttpClient;
import java.util.HashMap;
import java.util.Map;

public class StartUp {
    private static final String API_KEY = "SDgLmGHQl8WQyYnTik9ULi7f6UnFyro6FH6HYcI2";

    public static void startServer() {
        try {
            IFoodDao foodDao = createFoodDao();
            IFoodService foodService = createFoodService();
            Map<String, IProvider> providers = createProvidersMap(foodDao, foodService);
            CommandFactory commandFactory = createCommandFactory(providers);
            ICommandExecutor commandExecutor = createCommandExecutor(commandFactory);

            Server server = new Server(commandExecutor);
            server.start();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("An error occurred while starting server");
            e.printStackTrace();
        }
    }

    private static IFoodDao createFoodDao() throws IOException, ClassNotFoundException {
        FoodLoader foodLoader = new FoodLoader(Constants.FOODS_PATH);
        FoodReportLoader foodReportLoader = new FoodReportLoader(Constants.FOOD_REPORTS_PATH);
        BrandedFoodLoader brandedFoodLoader = new BrandedFoodLoader(Constants.BRANDED_FOODS_PATH);

        IFoodDao foodDao = new FoodDao(foodLoader, brandedFoodLoader, foodReportLoader);
        return foodDao;
    }

    private static FoodService createFoodService() {
        return new FoodService(HttpClient.newHttpClient(), API_KEY);
    }

    private static Map<String, IProvider> createProvidersMap(IFoodDao foodDao, IFoodService foodService) {
        Map<String, IProvider> providers = new HashMap<>();
        providers.put(Commands.GET_FOOD.getCommandName(), new FoodProvider(foodDao, foodService));
        providers.put(Commands.GET_FOOD_REPORT.getCommandName(), new FoodReportProvider(foodDao, foodService));
        providers.put(Commands.GET_FOOD_BY_BARCODE.getCommandName(), new BrandedFoodProvider(foodDao));

        return providers;
    }

    private static CommandFactory createCommandFactory(Map<String, IProvider> providers) {
        return new CommandFactory(providers);
    }

    private static CommandExecutor createCommandExecutor(CommandFactory commandFactory) {
        return new CommandExecutor(commandFactory);
    }
}

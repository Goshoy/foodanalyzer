package data;

import java.io.Serializable;
import java.util.Objects;

public class FoodReport implements Serializable {
    private static final long serialVersionUID = -6105666883184256056L;

    private String fdcId;
    private final String description;
    private final String ingredients;
    private final LabelNutrients labelNutrients;

    public FoodReport(String fdcId, String description, String dataType, String ingredients, LabelNutrients labelNutrients) {
        this.fdcId = fdcId;
        this.description = description;
        this.ingredients = ingredients;
        this.labelNutrients = labelNutrients;
    }

    public String getFdcId() {
        return fdcId;
    }

    public String getDescription() {
        return description;
    }

    public String getIngredients() {
        return ingredients;
    }

    public LabelNutrients getLabelNutrients() {
        return labelNutrients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoodReport that = (FoodReport) o;
        return Objects.equals(description, that.description) &&
                Objects.equals(ingredients, that.ingredients) &&
                Objects.equals(labelNutrients, that.labelNutrients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, ingredients, labelNutrients);
    }

    @Override
    public String toString() {
        StringBuilder report = new StringBuilder();

        report.append("******* Food Report *******")
                .append(System.lineSeparator())
                .append("description: ")
                .append(description)
                .append(System.lineSeparator())
                .append("ingredients: ")
                .append(ingredients)
                .append(System.lineSeparator())
                .append("fdcId: ")
                .append(fdcId)
                .append(System.lineSeparator())
                .append(labelNutrients);

        return report.toString();
    }
}

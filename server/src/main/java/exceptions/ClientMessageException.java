package exceptions;

public class ClientMessageException extends Exception {
    public ClientMessageException(String message) {
        super(message);
    }
}

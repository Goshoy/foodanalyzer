package loader;

import data.Food;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FoodLoader extends Loader {

    public FoodLoader(String filePath) {
        super(filePath);
    }

    public Set<Food> load() throws IOException, ClassNotFoundException {
        final List<Path> filePaths = super.getPaths(super.getFilePath());
        final Set<Food> result = new HashSet<>();

        for (final Path p : filePaths) {
            try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(p))) {
                result.add((Food) ois.readObject());
            }
        }

        return result;
    }
}

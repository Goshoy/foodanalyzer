package loaders;

import data.FoodReport;
import loader.FoodReportLoader;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FoodReportLoaderTest {
    private static final String FOOD_REPORT_LOADER_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/testloaders/report";
    private static final String FOOD_REPORT_LOADER_EMPTY_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/testloaders/reportempty";

    @Test
    public void testFoodReportLoaderWithNonEmptyDir() throws IOException, ClassNotFoundException {
        FoodReportLoader foodReportLoader = new FoodReportLoader(FOOD_REPORT_LOADER_PATH);

        Map<String, FoodReport> loadedFoods = foodReportLoader.load();
        int expectedNumberOfFoods = 2;

        assertEquals(loadedFoods.size(), expectedNumberOfFoods);
    }

    @Test
    public void testFoodReportLoaderWithEmptyDir() throws IOException, ClassNotFoundException {
        FoodReportLoader foodReportLoader = new FoodReportLoader(FOOD_REPORT_LOADER_EMPTY_PATH);

        Map<String, FoodReport> loadedFoods = foodReportLoader.load();

        assertTrue(loadedFoods.isEmpty());
    }
}

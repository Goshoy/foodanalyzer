package commands;

import exceptions.ClientMessageException;
import providers.IProvider;

import java.io.IOException;
import java.util.List;

public class GetFoodCommand extends AbstractCommand {
    public GetFoodCommand(IProvider provider) {
        super(provider);
    }

    @Override
    public String execute(List<String> args) throws IOException, ClientMessageException {
        String searchQUery = String.join(" ", args);
        return provider.provide(searchQUery);
    }
}

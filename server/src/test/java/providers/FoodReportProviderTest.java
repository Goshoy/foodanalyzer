package providers;

import dao.IFoodDao;
import data.FoodReport;
import exceptions.ClientMessageException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import service.IFoodService;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FoodReportProviderTest {
    @Mock
    IFoodDao foodDao;
    @Mock
    IFoodService foodService;

    FoodReportProvider foodReportProvider;

    @Before
    public void setUp() {
        this.foodReportProvider = new FoodReportProvider(foodDao, foodService);
    }

    @Test
    public void testGettingFoodReportInCacheReturnsNonEmptyStringAndCallsMethodsExactTime() throws IOException, ClientMessageException {
        when(foodDao.containsFoodReport(any())).thenReturn(true);
        when(foodDao.getFoodReport(any())).thenReturn(new FoodReport("1234", "name", "normal", null, null));

        String foods = foodReportProvider.provide("name");

        verify(foodDao, times(1)).containsFoodReport(any());
        verify(foodDao, times(1)).getFoodReport(any());
        assertNotNull(foods);
    }

    @Test
    public void testGettingNotCachedFoodReportReturnsNonEmptyStringAndCallsMethodsExactTime() throws IOException, ClientMessageException {
        when(foodDao.containsFoodReport(any())).thenReturn(false);
        when(foodService.getFoodReport(any())).thenReturn(new FoodReport("12345", "description", "normal", null, null));
        String foods = foodReportProvider.provide("name");

        verify(foodDao, times(1)).containsFoodReport(any());
        verify(foodService, times(1)).getFoodReport(any());
        assertNotNull(foods);
    }
}

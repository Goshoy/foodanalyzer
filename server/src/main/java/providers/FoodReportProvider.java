package providers;

import dao.IFoodDao;
import data.FoodReport;
import exceptions.ClientMessageException;
import service.IFoodService;

import java.io.IOException;

public class FoodReportProvider extends AbstractFoodProvider {

    public FoodReportProvider(IFoodDao foodDao, IFoodService foodService) {
        super(foodDao, foodService);
    }

    @Override
    public String provide(String criterion) throws IOException, ClientMessageException {
        if (foodDao.containsFoodReport(criterion)) {
            return foodDao.getFoodReport(criterion).toString();
        }

        FoodReport foodReport = foodService.getFoodReport(criterion);
        foodDao.cacheFoodReport(foodReport);

        return foodReport.toString();
    }

}

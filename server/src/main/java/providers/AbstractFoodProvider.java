package providers;

import dao.IFoodDao;
import service.IFoodService;

public abstract class AbstractFoodProvider implements IProvider {
    protected final IFoodDao foodDao;
    protected final IFoodService foodService;

    public AbstractFoodProvider(IFoodDao foodDao, IFoodService foodService) {
        this.foodDao = foodDao;
        this.foodService = foodService;
    }
}
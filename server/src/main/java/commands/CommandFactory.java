package commands;

import exceptions.NoSuchCommandException;
import providers.IProvider;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {

    private Map<String, AbstractCommand> commands;

    public CommandFactory(Map<String, IProvider> providers) {
        commands = new HashMap<>();
        initCommands(providers);
    }

    private void initCommands(Map<String, IProvider> providers) {
        commands.put(Commands.GET_FOOD.getCommandName(),
                new GetFoodCommand(providers.get(Commands.GET_FOOD.getCommandName())));
        commands.put(Commands.GET_FOOD_REPORT.getCommandName(),
                new GetFoodReportCommand(providers.get(Commands.GET_FOOD_REPORT.getCommandName())));
        commands.put(Commands.GET_FOOD_BY_BARCODE.getCommandName(),
                new GetFoodByBarcodeCommand(providers.get(Commands.GET_FOOD_BY_BARCODE.getCommandName())));
    }

    public AbstractCommand getCommand(final String command) throws NoSuchCommandException {
        if (!commands.containsKey(command)) {
            throw new NoSuchCommandException("No such command!");
        }

        return commands.get(command);
    }
}

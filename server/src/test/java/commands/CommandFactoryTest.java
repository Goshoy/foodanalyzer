package commands;

import exceptions.NoSuchCommandException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import providers.IProvider;

import java.util.Map;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CommandFactoryTest {

    @Mock
    Map<String, IProvider> providers;

    CommandFactory commandFactory;

    @Before
    public void setUp() {
        commandFactory = new CommandFactory(providers);
    }

    @Test
    public void testGettingGetFoodCommandShouldReturnGetFoodCommand() throws NoSuchCommandException {
        AbstractCommand cmd = commandFactory.getCommand(Commands.GET_FOOD.getCommandName());

        assertTrue(cmd instanceof GetFoodCommand);
    }

    @Test
    public void testGettingGetFoodReportCommandShouldReturnGetFoodReportCommand() throws NoSuchCommandException {
        AbstractCommand cmd = commandFactory.getCommand(Commands.GET_FOOD_REPORT.getCommandName());

        assertTrue(cmd instanceof GetFoodReportCommand);
    }

    @Test
    public void testGettingGetFoodByBarcodeCommandShouldReturnGetFoodByBarcodeCommand() throws NoSuchCommandException {
        AbstractCommand cmd = commandFactory.getCommand(Commands.GET_FOOD_BY_BARCODE.getCommandName());

        assertTrue(cmd instanceof GetFoodByBarcodeCommand);
    }

    @Test(expected = NoSuchCommandException.class)
    public void testGettingInvalidCommandShouldThrowException() throws NoSuchCommandException {
        AbstractCommand cmd = commandFactory.getCommand("Invalid");
    }

}

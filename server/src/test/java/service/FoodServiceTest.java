package service;

import com.google.gson.Gson;
import data.BrandedFood;
import data.Food;
import data.FoodList;
import data.FoodReport;
import exceptions.ClientMessageException;
import exceptions.EmptyFoodNameException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FoodServiceTest {
    private static final String API_KEY = "TEST_KEY";
    private static final Integer STATUS_CODE_OK = 200;
    private static final Integer STATUS_CODE_SERVER_ERROR = 500;

    @Mock
    private HttpClient httpClientMock;
    @Mock
    private HttpResponse<String> httpResponseMock;

    private FoodService foodService;

    @Before
    public void setUp() throws IOException, InterruptedException {
        foodService = new FoodService(httpClientMock, API_KEY);

        when(httpClientMock.send(Mockito.any(HttpRequest.class),
                ArgumentMatchers.<HttpResponse.BodyHandler<String>>any())).thenReturn(httpResponseMock);
    }

    @Test(expected = EmptyFoodNameException.class)
    public void testTryingToGetProductWithEmptyFoodNameShouldThrowException() throws ClientMessageException {
        final String productName = "";

        foodService.getFoods(productName);
    }

    @Test(expected = EmptyFoodNameException.class)
    public void testTryingToGetProductWithFoodNameEqualToNullShouldThrowException() throws ClientMessageException {
        final String productName = null;

        foodService.getFoods(productName);
    }

    @Test
    public void testGettingFoodWithValidNameShouldReturnValidFood() throws ClientMessageException {
        final String productName = "Raffaello";
        final Food food = new Food("42562", productName, "normal");
        final FoodList foodList = new FoodList(1, 1, List.of(food));

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_OK);
        when(httpResponseMock.body()).thenReturn(new Gson().toJson(foodList));
        List<Food> foods = foodService.getFoods(productName);

        int expectedListSize = 1;
        assertEquals(expectedListSize, foods.size());
        assertEquals(foods.get(0), food);
    }

    @Test(expected = ClientMessageException.class)
    public void testGettingFoodWithInvalidFoodNameShouldThrowException() throws ClientMessageException {
        final String productName = "Invalid";

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_SERVER_ERROR);
        foodService.getFoods(productName);
    }

    @Test
    public void testSendAsyncIsCalledTheRightTimesHavingMultiplePages() throws ClientMessageException {
        final String productName = "Raffaello";
        final int currentPage = 1;
        final int totalPages = 5;
        final Food food = new Food("42562", productName, "normal");
        final FoodList foodList = new FoodList(currentPage, totalPages, List.of(food));

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_OK);
        when(httpResponseMock.body()).thenReturn(new Gson().toJson(foodList));
        when(httpClientMock.sendAsync(any(), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any())).thenAnswer(answer -> {
            CompletableFuture<HttpResponse<String>> future = new CompletableFuture<>();
            future.complete(httpResponseMock);
            return future;
        });
        foodService.getFoods(productName);

        int exprectedTimesAsyncCalled = 1;
        verify(httpClientMock, times(exprectedTimesAsyncCalled)).sendAsync(any(), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
    }

    @Test
    public void testGettingFoodReportWithValidFdcIdShouldReturnValidFoodReport() throws InterruptedException, IOException, ClientMessageException {
        final String fdcId = "42653";
        final FoodReport foodReport = new FoodReport(fdcId, "description", "normal", null, null);

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_OK);
        when(httpResponseMock.body()).thenReturn(new Gson().toJson(foodReport));
        final FoodReport expected = foodService.getFoodReport(fdcId);

        verify(httpClientMock, times(1)).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
        assertEquals(expected, foodReport);
    }

    @Test(expected = ClientMessageException.class)
    public void testGettingFoodReportWithInValidFdcIdShouldThrowException() throws InterruptedException, IOException, ClientMessageException {
        final String fdcId = "invalid";
        final FoodReport foodReport = new FoodReport(fdcId, "description", "normal", null, null);

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_SERVER_ERROR);
        foodService.getFoodReport(fdcId);

        verify(httpClientMock, times(1)).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
    }

    @Test
    public void testGettingBrandedFoodWithValidGtinUpcCodeShouldReturnValidBrandedFood() throws InterruptedException, IOException, ClientMessageException {
        final String gtinUpc = "123456789";
        final BrandedFood brandedFood = new BrandedFood("12345", "description", "Branded", gtinUpc);

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_OK);
        when(httpResponseMock.body()).thenReturn(new Gson().toJson(brandedFood));
        final BrandedFood expected = foodService.getBrandedFood(gtinUpc);

        verify(httpClientMock, times(1)).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
        assertEquals(expected, brandedFood);
    }

    @Test(expected = ClientMessageException.class)
    public void testGettingBrandedFoodWithInValidGtinUpcCodeShouldThrowException() throws InterruptedException, IOException, ClientMessageException {
        final String gtinUpc = "invalid";
        final BrandedFood brandedFood = new BrandedFood("12345", "description", "Branded", gtinUpc);

        when(httpResponseMock.statusCode()).thenReturn(STATUS_CODE_SERVER_ERROR);
        foodService.getBrandedFood(gtinUpc);

        verify(httpClientMock, times(1)).send(Mockito.any(HttpRequest.class), ArgumentMatchers.<HttpResponse.BodyHandler<String>>any());
    }
}

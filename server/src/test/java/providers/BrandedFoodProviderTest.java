package providers;

import dao.IFoodDao;
import data.BrandedFood;
import exceptions.BrandedFoodNotFoundException;
import exceptions.ClientMessageException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BrandedFoodProviderTest {
    @Mock
    IFoodDao foodDao;

    BrandedFoodProvider brandedFoodProvider;

    @Before
    public void setUp() {
        this.brandedFoodProvider = new BrandedFoodProvider(foodDao);
    }

    @Test
    public void testGettingBrandedFoodShouldReturnNonEmptyStringAndCallsMethodsExactTime() throws ClientMessageException {
        when(foodDao.containsBrandedFood(any())).thenReturn(true);
        when(foodDao.getBrandedFood(any())).thenReturn(new BrandedFood("1234", "name", "normal", null));

        String foods = brandedFoodProvider.provide("name");

        verify(foodDao, times(1)).containsBrandedFood(any());
        verify(foodDao, times(1)).getBrandedFood(any());
        assertNotNull(foods);
    }

    @Test(expected = BrandedFoodNotFoundException.class)
    public void testGettingNotAvailableBrandedFoodShouldThrowException() throws ClientMessageException {
        when(foodDao.containsBrandedFood(any())).thenReturn(false);

        brandedFoodProvider.provide("name");

        verify(foodDao, times(1)).containsBrandedFood(any());
    }
}

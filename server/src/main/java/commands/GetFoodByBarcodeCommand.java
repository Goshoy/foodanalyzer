package commands;

import com.google.zxing.NotFoundException;
import exceptions.ClientMessageException;
import exceptions.FailedToExtractBarcodeFromImgException;
import providers.IProvider;
import utils.BarcodeExtractor;

import java.io.IOException;
import java.util.List;

import static utils.ServerResponses.ERROR_EXTRACTING_BARCODE;

public class GetFoodByBarcodeCommand extends AbstractCommand {

    private static final int FLAG_POSITION = 0;
    private static final String SPLIT_REGEX = "=";
    private static final String CODE_FLAG = "--code";
    public static final String IMG_FLAG = "--img";

    public GetFoodByBarcodeCommand(IProvider provider) {
        super(provider);
    }

    @Override
    public String execute(List<String> args) throws IOException, ClientMessageException {
        String barcode = null;
        for (String arg : args) {
            String[] splitArgs = arg.split(SPLIT_REGEX);
            if (splitArgs.length > 1) {
                if (splitArgs[0].equals(CODE_FLAG)) {
                    barcode = splitArgs[1];
                }

                if (barcode == null && splitArgs[0].equals(IMG_FLAG)) {
                    try {
                        barcode = BarcodeExtractor.extractCode(splitArgs[1]);
                    } catch (NotFoundException | IOException e) {
                        throw new FailedToExtractBarcodeFromImgException(ERROR_EXTRACTING_BARCODE);
                    }
                }
            }
        }

        return provider.provide(barcode);
    }

}

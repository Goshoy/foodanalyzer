package providers;

import dao.IFoodDao;
import data.BrandedFood;
import data.Food;
import exceptions.ClientMessageException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import service.IFoodService;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FoodProviderTest {

    @Mock
    IFoodDao foodDao;
    @Mock
    IFoodService foodService;

    FoodProvider foodProvider;

    @Before
    public void setUp() {
        this.foodProvider = new FoodProvider(foodDao, foodService);
    }

    @Test
    public void testGettingFoodInCacheShouldReturnNonEmptyString() throws IOException, ClientMessageException {
        when(foodDao.containsFood(any())).thenReturn(true);
        when(foodDao.getFood(any())).thenReturn(Arrays.asList(new Food("1234", "name", "normal")));

        String foods = foodProvider.provide("name");

        verify(foodDao, times(1)).containsFood(any());
        verify(foodDao, times(1)).getFood(any());
        assertNotNull(foods);
    }

    @Test
    public void testGettingNotCachedFoodShouldReturnNonEmptyString() throws IOException, ClientMessageException {
        when(foodDao.containsFood(any())).thenReturn(false);
        when(foodService.getFoods(any())).thenReturn(Arrays.asList(new Food("12345", "description", "normal")));
        String foods = foodProvider.provide("name");

        verify(foodDao, times(1)).containsFood(any());
        verify(foodService, times(1)).getFoods(any());
        assertNotNull(foods);
    }

    @Test
    public void testGettingNotCachedFoodWithDataTypeBrandedReturnsNonEmptyStringAndCallsMethodsExactTime() throws IOException, ClientMessageException {
        when(foodDao.containsFood(any())).thenReturn(false);
        when(foodService.getFoods(any())).thenReturn(Arrays.asList(new Food("12345", "description", "Branded")));
        when(foodService.getBrandedFood(any())).thenReturn(new BrandedFood(null, null, null, null));
        String foods = foodProvider.provide("name");

        verify(foodDao, times(1)).containsFood(any());
        verify(foodService, times(1)).getFoods(any());
        verify(foodService, times(1)).getBrandedFood(any());
        assertNotNull(foods);
    }
}

package exceptions;

public class FoodApiServerException extends ClientMessageException {
    public FoodApiServerException(String message) {
        super(message);
    }
}

package exceptions;

public class NoArgumentsException extends ClientMessageException {
    public NoArgumentsException(String message) {
        super(message);
    }
}

package loader;

import data.FoodReport;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FoodReportLoader extends Loader {
    public FoodReportLoader(String filePath) {
        super(filePath);
    }

    public Map<String, FoodReport> load() throws IOException, ClassNotFoundException {
        final List<Path> filePaths = super.getPaths(super.getFilePath());
        final Map<String, FoodReport> foods = new HashMap<>();

        for (final Path p : filePaths) {
            try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(p))) {
                FoodReport foodReport = (FoodReport) ois.readObject();
                foods.put(foodReport.getFdcId(), foodReport);
            }
        }

        return foods;
    }

}

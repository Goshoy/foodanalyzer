package providers;

import exceptions.ClientMessageException;

import java.io.IOException;

public interface IProvider {
    public String provide(String criterion) throws ClientMessageException, IOException;
}

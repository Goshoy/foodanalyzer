package exceptions;

public class EmptyFoodNameException extends ClientMessageException {
    public EmptyFoodNameException(String message) {
        super(message);
    }
}

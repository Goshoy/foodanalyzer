package exceptions;

public class NoSuchCommandException extends ClientMessageException {
    public NoSuchCommandException(String message) {
        super(message);
    }
}

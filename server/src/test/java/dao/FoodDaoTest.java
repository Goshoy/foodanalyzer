package dao;

import data.BrandedFood;
import data.Food;
import data.FoodReport;
import loader.BrandedFoodLoader;
import loader.FoodLoader;
import loader.FoodReportLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FoodDaoTest {
    private static final String FOOD_LOADER_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/foodLoader";

    @Mock
    FoodLoader foodLoader;
    @Mock
    BrandedFoodLoader brandedFoodLoader;
    @Mock
    FoodReportLoader foodReportLoader;

    FoodDao foodDao;

    Food food1 = new Food("1234", "description", "normal");
    Food food2 = new Food("5678", "raffaello description", "normal");
    BrandedFood brandedFood = new BrandedFood("4567", "description", "Branded", "123456789");
    FoodReport foodReport = new FoodReport("4321", "desc1", "normal", null, null);

    @Before
    public void setUp() throws IOException, InterruptedException, ClassNotFoundException {
//        clean();
        when(foodLoader.load()).thenReturn(new HashSet<>(Arrays.asList(food1, food2)));
        when(brandedFoodLoader.load()).thenReturn(new HashMap<String, BrandedFood>(Map.of(brandedFood.getGtinUpc(), brandedFood)));
        when(foodReportLoader.load()).thenReturn(Map.of(foodReport.getFdcId(), foodReport));

//        when(foodLoader.getFilePath()).thenReturn(FOOD_LOADER_PATH);

        foodDao = new FoodDao(foodLoader, brandedFoodLoader, foodReportLoader);
    }

    private void clean() throws IOException {
        Files.walk(Paths.get(FOOD_LOADER_PATH)).filter(Files::isRegularFile).forEach(path -> {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void testGettingCachedFoodReturnsTheRightFood() throws IOException, ClassNotFoundException {
        List<Food> expected = foodDao.getFood(List.of("Raffaello"));
        int expectedListSize = 1;

        assertEquals(expected.size(), expectedListSize);
        assertEquals(expected.get(0), food2);
    }

    @Test
    public void testGettingCachedFoodReturnsTheRightNumberOfFoundFoods() throws IOException, ClassNotFoundException {
        List<Food> expected = foodDao.getFood(List.of("Description"));
        int expectedListSize = 2;

        assertEquals(expected.size(), expectedListSize);
    }

    @Test
    public void testGettingCachedFoodReturnsEmptyListWhenNoFoodsFound() throws IOException, ClassNotFoundException {
        List<Food> expected = foodDao.getFood(List.of("Invalid"));
        int expectedListSize = 0;

        assertEquals(expected.size(), expectedListSize);
    }

//    @Test
//    public void testAddingFoodCreatesFile() throws IOException, ClassNotFoundException {
//        Food newFood = new Food("7689", "new food", "normal");
//        foodDao.cacheFood(newFood);
//
//        List<Food> expected = foodDao.getFood(List.of("new Food"));
//
//        assertFalse(expected.isEmpty());
//        assertEquals(expected.get(0), newFood);
//    }

    @Test
    public void testGettingCachedFoodReportReturnsTheRightFoodReport() throws IOException, ClassNotFoundException {
        FoodReport expected = foodDao.getFoodReport("4321");

        assertNotNull(expected);
        assertEquals(expected, foodReport);
    }

    @Test
    public void testGettingCachedFoodReportReturnsNullWhenNoFoodReportFound() throws IOException, ClassNotFoundException {
        FoodReport expected = foodDao.getFoodReport("Invalid");

        assertNull(expected);
    }

    @Test
    public void testGettingCachedBrandedFoodReturnsTheRightBrandedFood() throws IOException, ClassNotFoundException {
        BrandedFood expected = foodDao.getBrandedFood("123456789");

        assertNotNull(expected);
        assertEquals(expected, brandedFood);
    }

    @Test
    public void testGettingCachedBrandedFoodReturnsNullWhenNoBrandedFoodFound() throws IOException, ClassNotFoundException {
        BrandedFood expected = foodDao.getBrandedFood("Invalid");

        assertNull(expected);
    }

    @Test
    public void testCheckingIfFoodIsContainedInCacheShouldReturnTrue() {
        boolean contained = foodDao.containsFood(List.of("description"));

        assertTrue(contained);
    }

//    @Test
//    public void testCheckingIfFoodIsContainedInCacheWithPartialNameShouldReturnFalse() {
//        boolean contained = foodDao.containsFood(List.of("raffaello"));
//
//        assertFalse(contained);
//    }

}

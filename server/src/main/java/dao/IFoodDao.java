package dao;

import data.BrandedFood;
import data.Food;
import data.FoodReport;

import java.io.IOException;
import java.util.List;

public interface IFoodDao {
    public void cacheFood(final Food food) throws IOException;

    public void cacheFoodReport(final FoodReport foodReport) throws IOException;

    public void cacheBrandedFood(final BrandedFood brandedFood) throws IOException;

    public List<Food> getFood(final List<String> foodName);

    /**
     * Gets cached food report
     *
     * @param fdcId unique food identifier
     * @return FoodReport
     */
    public FoodReport getFoodReport(final String fdcId);

    /**
     * Gets cached branded food
     *
     * @param gtinUpc (Global Trade Item Number)
     * @return BrandedFood
     */
    public BrandedFood getBrandedFood(final String gtinUpc);

    public boolean containsFood(final List<String> foodName);

    public boolean containsBrandedFood(final String gtinUpc);

    public boolean containsFoodReport(String fdcId);
}

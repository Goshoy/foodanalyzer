package utils;

public class ServerResponses {
    public static String EMPTY_FOOD_NAME = "Empty food name is not allowed!";
    public static String SERVER_RETRIEVE_FAILURE = "Failed to retrieve food.";
    public static String MISSING_ARGUMENTS = "Command arguments missing!";
    public static String ERROR_EXTRACTING_BARCODE = "Error occured during extraction of barcode from image.";
}

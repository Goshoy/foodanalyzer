package data;

import java.io.Serializable;
import java.util.Objects;

public class Food implements Serializable {
    private static final long serialVersionUID = -8351165486151183917L;

    private final String fdcId;
    private final String description;
    private final String dataType;

    public Food(String fdcId, String description, String dataType) {
        this.fdcId = fdcId;
        this.description = description;
        this.dataType = dataType;
    }

    public String getFdcId() {
        return fdcId;
    }

    public String getDescription() {
        return description;
    }

    public String getDataType() {
        return dataType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Food food = (Food) o;
        return Objects.equals(fdcId, food.fdcId) &&
                Objects.equals(description, food.description) &&
                Objects.equals(dataType, food.dataType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fdcId, description, dataType);
    }

    @Override
    public String toString() {
        StringBuilder food = new StringBuilder();

        food.append("******* Food Details *******")
                .append(System.lineSeparator())
                .append("fdcId: ")
                .append(fdcId)
                .append(System.lineSeparator())
                .append("description: ")
                .append(description)
                .append(System.lineSeparator())
                .append("data type: ")
                .append(dataType)
                .append(System.lineSeparator());

        return food.toString();
    }
}

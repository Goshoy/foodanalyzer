package utils;

public class Constants {
    public static final String FOODS_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/main/resources/cache/foods/";
    public static final String BRANDED_FOODS_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/main/resources/cache/branded/";
    public static final String FOOD_REPORTS_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/main/resources/cache/reports/";

    public static final String WHITE_SPACE_REGEX = " ";
    public static final String QUERY_DELIMITER = "%20";

    public static final String DATA_TYPE_BRANDED = "Branded";

}

package loaders;

import data.BrandedFood;
import loader.BrandedFoodLoader;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BrandedFoodLoaderTest {
    private static final String BRANDED_LOADER_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/testloaders/branded";
    private static final String BRANDED_LOADER_EMPTY_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/testloaders/brandedempty";

    @Test
    public void testBrandedLoaderWithNonEmptyDir() throws IOException, ClassNotFoundException {
        BrandedFoodLoader brandedFoodLoader = new BrandedFoodLoader(BRANDED_LOADER_PATH);

        Map<String, BrandedFood> loadedFoods = brandedFoodLoader.load();
        int expectedNumberOfFoods = 4;

        assertEquals(loadedFoods.size(), expectedNumberOfFoods);
    }

    @Test
    public void testBrandedLoaderWithEmptyDir() throws IOException, ClassNotFoundException {
        BrandedFoodLoader brandedFoodLoader = new BrandedFoodLoader(BRANDED_LOADER_EMPTY_PATH);

        Map<String, BrandedFood> loadedFoods = brandedFoodLoader.load();

        assertTrue(loadedFoods.isEmpty());
    }
}

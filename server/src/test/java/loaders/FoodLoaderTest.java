package loaders;

import data.Food;
import loader.FoodLoader;
import org.junit.Test;

import java.io.IOException;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FoodLoaderTest {
    private static final String FOOD_LOADER_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/testloaders/food";
    private static final String FOOD_LOADER_EMPTY_PATH = "/home/goshoy/IdeaProjects/FoodAnalyzerServerMaven/src/test/resources/testloaders/foodempty";

    @Test
    public void testFoodLoaderWithNonEmptyDir() throws IOException, ClassNotFoundException {
        FoodLoader foodLoader = new FoodLoader(FOOD_LOADER_PATH);

        Set<Food> loadedFoods = foodLoader.load();
        int expectedNumberOfFoods = 3;

        assertEquals(loadedFoods.size(), expectedNumberOfFoods);
    }

    @Test
    public void testFoodLoaderWithEmptyDir() throws IOException, ClassNotFoundException {
        FoodLoader foodLoader = new FoodLoader(FOOD_LOADER_EMPTY_PATH);

        Set<Food> loadedFoods = foodLoader.load();

        assertTrue(loadedFoods.isEmpty());
    }
}

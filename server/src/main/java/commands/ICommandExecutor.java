package commands;

import exceptions.ClientMessageException;

import java.io.IOException;

public interface ICommandExecutor {
    public String execute(String input) throws IOException, ClientMessageException;
}

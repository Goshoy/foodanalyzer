package providers;

import dao.IFoodDao;
import data.BrandedFood;
import data.Food;
import exceptions.ClientMessageException;
import service.IFoodService;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static utils.Constants.*;

public class FoodProvider extends AbstractFoodProvider {

    public FoodProvider(IFoodDao foodDao, IFoodService foodService) {
        super(foodDao, foodService);
    }

    @Override
    public String provide(String criterion) throws ClientMessageException, IOException {
        List<String> foodWords = Arrays.asList(criterion.split(WHITE_SPACE_REGEX));
        if (foodDao.containsFood(foodWords)) {
            return foodDao.getFood(foodWords).toString();
        }

        List<Food> foods = foodService.getFoods(String.join(QUERY_DELIMITER, foodWords));
        for (Food food : foods) {
            if (food.getDataType().equals(DATA_TYPE_BRANDED)) {
                // Remove BrnadedFood class
                BrandedFood brandedFood = foodService.getBrandedFood(food.getFdcId());
                foodDao.cacheBrandedFood(brandedFood);
            }

            foodDao.cacheFood(food);
        }

        return foods.toString();
    }
}
